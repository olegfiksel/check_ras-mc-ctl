#!/usr/bin/perl
use strict;
use warnings;

use Getopt::Long;
use FindBin qw($Bin);
use lib $Bin.'/lib';
use Functions;

my $version = 'v1.0.1';

my $command = 'ras-mc-ctl --error-count';
# Debug
#my $command = 'cat tests/1/*.txt';
my $verbose = 0;
my $help = 0;

my $db = {};

GetOptions (
  "command=s" => \$command,
	"verbose" => \$verbose,
	"help" => \$help,
) or die("Error in command line arguments\n");

usage() if $help;

main();
exit 3;

sub usage{
	die <<EOF;
$0 $version
Usage:
	$0 [--command '$command'] [--verbose] [--help]

EOF
}

sub main{
  my @output = `$command`;
  my $exit_code = ($? >> 8);
  die "Command returned non-zero return code!" if $exit_code != 0;

  die "Command returned empty output!" if @output < 2;

  my ($ret_code,$ret_string) = Functions::check_ras_mc_ctl(\@output);
  print $ret_string;
  print join($/, @output).$/ if $verbose;
  exit $ret_code;
}
