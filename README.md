[![codecov](https://codecov.io/gl/olegfiksel/check_ras-mc-ctl/branch/master/graph/badge.svg)](https://codecov.io/gl/olegfiksel/check_ras-mc-ctl)

# check_ras-mc-ctl

Icinga / Nagios check for monitoring the EDAC (Error Detection And Correction) values using ras-mc-ctl.

```
Usage:
        ./check_ras-mc-ctl.pl [--command 'ras-mc-ctl --error-count'] [--verbose] [--help]
```

![](/images/screenshot01.png)