#!/usr/bin/perl
use strict;
use warnings;

package Functions;

my $ret_codes_map = {
    0 => 'OK',
    1 => 'WARNING',
    2 => 'CRITICAL',
};

sub check_ras_mc_ctl{
    my $cmd_output = shift;
    my $ret_code = 0;
    my $ret_string = "";
    my $CEs = 0;
    my $UEs = 0;
    
    my $parsed_lines = 0;
    foreach my $line (@$cmd_output){
      chomp $line;
      if($line =~ /^(\S+)\s+(\d+)\s+(\d+)/){
        my $label = $1;
        $parsed_lines++;
        my $ce = $2;
        my $ue = $3;
        $CEs += $ce if $ce > 0;
        $UEs += $ue if $ue > 0;
      }
    }
    if($parsed_lines == 0){
        return(50, "Cannot parse command's output!");
    }
    
    if($UEs > 0){
      $ret_code = 2;
    }
    elsif($CEs > 0){
      $ret_code = 1;
    }
    
    $ret_string = $ret_codes_map->{$ret_code}.": Unrecovered Errors: $UEs Corrected Errors: $CEs".$/;
    
    return $ret_code, $ret_string;
}

1;