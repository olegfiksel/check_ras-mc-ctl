use strict;
use warnings;
 
use Test::Simple tests => 4;

use Functions;
use Data::Dumper;

my $db = [
    {
        description => "OK - all ok",
        input => <<EOF,
Label                   CE      UE
mc#3csrow#3channel#1    0       0
mc#3csrow#0channel#0    0       0
mc#2csrow#0channel#0    0       0
mc#2csrow#2channel#0    0       0
mc#2csrow#1channel#0    0       0
mc#2csrow#1channel#1    0       0
mc#3csrow#2channel#1    0       0
mc#1csrow#0channel#0    0       0
mc#1csrow#0channel#1    0       0
mc#3csrow#1channel#1    0       0
mc#0csrow#2channel#0    0       0
mc#3csrow#3channel#0    0       0
mc#2csrow#3channel#1    0       0
mc#1csrow#2channel#0    0       0
mc#2csrow#2channel#1    0       0
mc#0csrow#0channel#0    0       0
mc#1csrow#2channel#1    0       0
mc#3csrow#1channel#0    0       0
mc#3csrow#2channel#0    0       0
mc#0csrow#0channel#1    0       0
mc#2csrow#3channel#0    0       0
mc#2csrow#0channel#1    0       0
mc#3csrow#0channel#1    0       0
mc#0csrow#2channel#1    0       0
EOF
        expect_code => 0,
        expect_string => 'OK: Unrecovered Errors: 0 Corrected Errors: 0'.$/,
    },
    {
        description => "WARNING - some CEs",
        input => <<EOF,
Label               	CE	UE
mc#3csrow#3channel#1	0	0
mc#3csrow#0channel#0	0	0
mc#2csrow#0channel#0	0	0
mc#2csrow#2channel#0	0	0
mc#2csrow#1channel#0	0	0
mc#2csrow#1channel#1	0	0
mc#3csrow#2channel#1	0	0
mc#1csrow#0channel#0	0	0
mc#1csrow#0channel#1	0	0
mc#3csrow#1channel#1	0	0
mc#0csrow#2channel#0	0	0
mc#3csrow#3channel#0	0	0
mc#2csrow#3channel#1	0	0
mc#1csrow#2channel#0	0	0
mc#2csrow#2channel#1	0	0
mc#0csrow#0channel#0	2	0
mc#1csrow#2channel#1	0	0
mc#3csrow#1channel#0	0	0
mc#3csrow#2channel#0	0	0
mc#0csrow#0channel#1	0	0
mc#2csrow#3channel#0	0	0
mc#2csrow#0channel#1	0	0
mc#3csrow#0channel#1	0	0
mc#0csrow#2channel#1	0	0
EOF
        expect_code => 1,
        expect_string => 'WARNING: Unrecovered Errors: 0 Corrected Errors: 2'.$/,
    },
    {
        description => "CRITICAL - some CEs and UEs",
        input => <<EOF,
Label               	CE	UE
mc#3csrow#3channel#1	0	0
mc#3csrow#0channel#0	0	0
mc#2csrow#0channel#0	0	0
mc#2csrow#2channel#0	0	0
mc#2csrow#1channel#0	0	0
mc#2csrow#1channel#1	0	0
mc#3csrow#2channel#1	0	0
mc#1csrow#0channel#0	0	0
mc#1csrow#0channel#1	4	2
mc#3csrow#1channel#1	0	0
mc#0csrow#2channel#0	0	0
mc#3csrow#3channel#0	0	0
mc#2csrow#3channel#1	0	0
mc#1csrow#2channel#0	0	0
mc#2csrow#2channel#1	0	0
mc#0csrow#0channel#0	2	1
mc#1csrow#2channel#1	0	0
mc#3csrow#1channel#0	0	0
mc#3csrow#2channel#0	0	0
mc#0csrow#0channel#1	0	0
mc#2csrow#3channel#0	0	0
mc#2csrow#0channel#1	0	0
mc#3csrow#0channel#1	0	0
mc#0csrow#2channel#1	0	0
EOF
        expect_code => 2,
        expect_string => 'CRITICAL: Unrecovered Errors: 3 Corrected Errors: 6'.$/,
    },
    {
        description => "Non-parsable content",
        input => <<EOF,
Some non-parsable content
EOF
        expect_code => 50,
        expect_string => "Cannot parse command's output!",
    },
];

foreach my $test (@$db){
    my @input = split /\n/, $test->{input};
    my ($result_code, $result_string) = Functions::check_ras_mc_ctl(\@input);
    ok( $result_code == $test->{expect_code} && $result_string eq $test->{expect_string}, $test->{description} );
}